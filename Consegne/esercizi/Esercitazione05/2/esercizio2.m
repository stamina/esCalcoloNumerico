function [] = esercizio2(in, fine, spazio)
equisp = in:spazio:fine;
for n = 1 : length(equisp)
    xPunti = linspace(-1,1,equisp(n));
    y1 = ones(1,500);
    y2 = ones(1,500);
    i = 1:equisp(n);
    nodi = cos(((2*i + 1)*pi)/(2*equisp(n) + 2));
    x = linspace(-1,1,500);
    for i = 1 : 500
        for j = 1: length(xPunti)
        y1(i) = y1(i) *abs((x(i) - xPunti(j)));
        y2(i) =  y2(i) * abs((x(i) - nodi(j)));
        end
    end
    c(n) = figure;
    plot(x,y1,'r',x,y2,'b');
    title(strcat('N = ',num2str(equisp(n))));
    grid on;
    legend('Con nodi equispaziati)','Con nodi di Chebyshev');
    name = sprintf('esercitazione05-02_graficoN=%d',equisp(n));
    saveas(c(n),strcat(name,'.fig'),'fig');
    
end
end

