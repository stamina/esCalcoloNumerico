function [ soluzioni ] = risolviTridiagonale(MTrid, tnoti)
    a = diag(MTrid);
    b = diag(MTrid,-1);
    c = diag(MTrid,1);
    alfa(1) = a(1);
    for i = 2 : size(MTrid)
        beta(i-1) = b(i-1)/alfa(i-1);
        alfa(i) = a(i) - beta(i -1)* c(i-1);
    end
    L = eye(size(MTrid)) + diag(beta,-1);
    U = diag(alfa) + diag(c,+1);
    
    y = L\tnoti;
    soluzioni = U\y;

end

