function [ B, A] = costruzioneB(a, c)
% Passati 'a' e 'c', vettori t.c. length(a) = length(c) + 1.
% creiamo la matrice B.
    p = zeros(length(a),1);
    q = zeros(length(c),1);
    p(1) = sqrt(a(1));
    for i = 2 : length(a)
       q(i-1) = c(i-1) ./ p(i-1);
       p(i) = sqrt(a(i) - (q(i-1))^2);
    end
    B = diag(p) + diag(q,-1);
    A = B*B';
end

