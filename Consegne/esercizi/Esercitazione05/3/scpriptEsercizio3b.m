c = figure;
colors = ['m', 'c', 'g', 'b', 'k', 'r'];
for m = 1:6
 x1 = linspace(-m,0,100);
 x2 = linspace(0,m,100);
 x = [x1 x2];
 y1 = (m - ((x1.^2)./m)).^m;
 y2 = (((x2.^2)./m)+m).^m;
 y = [y1 y2];
 hold on;
 plot(x,y, colors(m));
 grid on;
 
end

legend('con m = 1', 'con m = 2','con m = 3',...
      'con m = 4','con m = 5', 'con m = 6');
  title('Esercitazione 5 n. 3 (parte b)');
saveas(c,'GraficoB','fig');