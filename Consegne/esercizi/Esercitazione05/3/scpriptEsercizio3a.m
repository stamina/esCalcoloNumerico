c = figure;
for m = 1:6
 x1 = linspace(-m,0,100);
 x2 = linspace(0,m,100);
 x = [x1 x2];
 y1 = (m - ((x1.^2)./m)).^m;
 y2 = (((x2.^2)./m)+m).^m;
 y = [y1 y2];
 subplot(2,3,m);
 plot(x,y);
 grid on;
 legend(sprintf('con m = %d', m));
end
saveas(c,'graficoA','fig');