function [ pgreco, scostam] = approssPi(n)
    pgreco = 0;
    val = 0;
    for i = 0 : n
        val = 16^(-i) * ((4/(8*i + 1)) - (2/(8 * i + 4)) ...
            - (1/(8*i + 5)) - (1/(8*i+6)));
        pgreco = pgreco + val;
    end
    scostam = pi - pgreco;
end

