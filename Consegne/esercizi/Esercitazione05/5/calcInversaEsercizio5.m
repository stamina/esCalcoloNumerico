function [ inversa ] = calcInversaEsercizio5(n)
% Calcolo della matrice inversa tale che ogni el su A(i,i) = 1 e
% su A(i,i+1) = 2, mentre gli altri elementi nulli.
% n corrisponde alla dimensione della matrice di dimensione nxn.

inversa = eye(n,n);
for i = 2 : n
    inversa = inversa + diag(ones(n-i+1,1)* (-2)^(i-1),i-1);
end

% La funzione e' stata implementata in modo tale da avere un basso costo.
% Matematicamente si e' riscontrato che la matrice inversa ha sempre la
% medesima forma, pertanto non e' necessario svolgere calcoli
% particolarmente complessi.
end

