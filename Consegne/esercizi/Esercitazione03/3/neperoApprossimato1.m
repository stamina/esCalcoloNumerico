function [conteggio, nepero_approssimato] = neperoApprossimato1(x)
    numero_di_nepero = exp(x);
    conteggio = 1;
    nepero_approssimato = 1;
    while (abs(numero_di_nepero - nepero_approssimato) > 0.000001)
            nepero_approssimato = nepero_approssimato + x^conteggio/factorial(conteggio);
            conteggio = conteggio + 1;
    end
end

