function [conteggio, nepero_approssimato] = neperoApprossimato2(x)
	if x < 0
	    [conteggio, nepero_approssimato] = neperoApprossimato1(-x);
	    nepero_approssimato = 1./nepero_approssimato;
	else
	   [conteggio, nepero_approssimato] = esercitazione03es03(x);
	end
end
