function [risultato, errore] = confronto(x)
    risultato = [];
    numero_di_nepero1 = exp(x);
    numero_di_nepero2 = exp(abs(x));
    conteggio = 1;
    nepero_approssimato1 = 1;
    nepero_approssimato2 = 1;
    while (abs(numero_di_nepero1 - nepero_approssimato1) > 0.000001 ...
            || abs(numero_di_nepero2 - nepero_approssimato2) > 0.000001)
        
            nepero_approssimato1 = nepero_approssimato1 + ...
                x^conteggio/factorial(conteggio);            
            nepero_approssimato2 = nepero_approssimato2 + ...
                (abs(x))^conteggio/factorial(conteggio);            
            risultato = [risultato; ...
                nepero_approssimato1 nepero_approssimato2];
            conteggio = conteggio + 1;
    end
    if x < 0
        risultato = [risultato(:,1) 1./risultato(:,2)];  
    end
    errore = abs((risultato - numero_di_nepero1)./numero_di_nepero1);
end

