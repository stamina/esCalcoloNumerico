x = -5:9
x =
  Columns 1 through 12
    -5    -4    -3    -2    -1     0     1     2     3     4     5     6
  Columns 13 through 15
     7     8     9

massimo = max(x)
massimo =
     9

minimo = min(x)
minimo =
    -5

minimoAssoluto = min(abs(x))
minimoAssoluto =
     0

massimoAssoluto = max(abs(x))
massimoAssoluto =
     9

somma = sum(x)
somma =
    30
    
sommaAssoluto = sum(abs(x))
sommaAssoluto =
    60
