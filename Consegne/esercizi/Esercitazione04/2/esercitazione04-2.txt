A = hilb(1000);
B = rand(1000);
x = ones(1000,1);
b = A * x;
y = ones(1000,1);
c = B * y;

%%%%%%%%%%%%%%%%%%%%

x = [];
x = A\b;
y = [];
y = B\c;

%%%%%%%%%%%%%%%%%%%%

condA = cond(A)
condA =
   2.5685e+21

condB = cond(B)
condB =
   5.3529e+05

condA = cond(A)
condA =
   2.5685e+21

condB = cond(B)
condB =
   5.3529e+05

B(10)
ans =
    0.9649

erroreA = abs(1 - x)/1;
erroreB = abs(1 - y)/1;

maggErroreA = max(erroreA)
maggErroreA =
   7.6412e+03

maggErroreB = max(erroreB)
maggErroreB =
   2.9911e-11

% Osservando i risultati e' possibile osservare che vi e' rapporto fra l'errore
% ed il condizionamento della matrice.
% In questo caso e' possibile infatti vedere che piu' grande e' il condizionamento
% della matrice, maggiore e' l'errore aspettato.
% Si consideri che il condizionamento non necessariamente e' indice dell'effettivo
% errore, ma della possibilita' della sua presenza nel caso i dati in ingresso
% siano alterati, anche leggermente.