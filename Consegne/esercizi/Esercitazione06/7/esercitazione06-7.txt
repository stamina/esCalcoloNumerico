xx = [1.5 4 5 5.5 8];
yy = sin(xx)+log(xx)-1;

polinomio = polyfit(xx,yy,4);
x = 1:0.001:9;

ypol = polyval(polinomio,xx);
y = sin(x) + log(x) - 1;

hold on
plot(x,y,'r')
plot(x,ypol,'b')
plot(xx,yy,'go')
diary off
