function [y] = polinInterp(n)
    punti = linspace(-1,1,n+1);
    ypunti = 1./(1+(25.*(punti.^2)));
    polinomio = polyfit(punti,ypunti,n);
    x = -1 :0.001:1;
    y = polyval(polinomio,x);
end

