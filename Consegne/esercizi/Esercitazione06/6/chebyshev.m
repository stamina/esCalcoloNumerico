function [ cheb ] = chebyshev(a, b, n)
    cheb = zeros(1,n);
    for i = 1:n
        cheb(i) = ((a+b)/2)+((b-a)/2)*cos(((2*i-1)*pi)/(2*n + 2));
    end
end