x = -1:0.5:1;
y = [0.5 0.8 1 0.8 0.5];
splvinc = csape(x,y,'complete',[1/2 -1/2]);
splnotknot = csape(x,y,'not-a-knot');
xx = -1:0.01:1;
yvinc = ppval(splvinc,xx);
ynotknot = ppval(splnotknot,xx);
hold on;
plot(xx,yvinc,'r')
plot(xx,ynotknot,'g')
plot(x,y,'bo')
