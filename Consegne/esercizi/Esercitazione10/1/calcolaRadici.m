function [ alfa ] = calcolaRadici(p, x,  toll, maxPassi)
% prende in input i coeff. di un polinomio e restituisce le sue radici
    pPrimo = polyder(p);
    alfa = [];
    for passo = 1 : length(p) -1; % numero di radici
        precedente = -Inf;
        attuale = x;
        prove = 0;
        while (prove <= maxPassi && abs(precedente - attuale) > toll)
            precedente = attuale;
            attuale = precedente - polyval(p,precedente)/...
                (polyval(pPrimo,precedente) - polyval(p,precedente) ...
                  * sum(1./(precedente - alfa)));
            prove = prove + 1;
        end
        if (prove >= maxPassi)
            return;
        end
        alfa(passo) = attuale;
    end
    

end

