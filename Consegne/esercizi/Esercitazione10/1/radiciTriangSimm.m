function [ autovalori, p1] = radiciTriangSimm(A,toll,maxpassi)
% Trovo autovalori con metodo newtoniano di matrice simm. e triang.
    alfa = diag(A)';
    beta = diag(A,-1)';
    p0 = [1];
    p1 = [-1 alfa(1)];
        for i = 2:length(A)
            p2 = conv(p1,[-1 alfa(i)]);
            p3 = - beta(i-1).^2 * p0;
            m = length(p2) - length(p3);
            p3 = [zeros(1,m) p3];
            p0 = p1;
            p1 = p2 + p3;
        end
    x0 = ricercaX0(A)-eps;    
    autovalori = calcolaRadici(p1,x0,toll,maxpassi);   
end

