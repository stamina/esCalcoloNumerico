function [ minv] = ricercaX0(A)
% data la matrice A, cerca un valore reale
% piu' piccolo di tutti gli autovalori
mins = zeros(1,length(A));
    for i = 1:length(A)
        mins(i) = A(i,i) - abs(sum(A(i,:))) + abs(A(i,i));
    end
minv = min(mins);
end

