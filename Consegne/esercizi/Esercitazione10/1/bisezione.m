function [alfa, nIterazioni] = bisezione(f,a,b,toll,maxIterazioni)
    nIterazioni = 0;
    while ((abs(a-b) > toll) && (nIterazioni < maxIterazioni))
        nIterazioni = nIterazioni +1;
        alfa = (b+a)/2;
        if (f(alfa) == 0)
            return;
        end
        if (f(a)*f(alfa)) < 0
            b = alfa;
        else
            a = alfa;
        end
    end
end

