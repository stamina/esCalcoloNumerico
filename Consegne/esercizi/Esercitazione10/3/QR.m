function [Q, R] = QR(A)
% Fattorizza una matrice A usando metodo QR
    Q = eye(length(A));
    for i = 1: length(A)
        x = A(i:length(A),i);
        s = norm(x,2);
        u = x;
        if (u(1) > 0)
            u(1) = x(1) + s;
        else
            u(1) = x(1) -s;
        end
        u = [zeros(length(A) - length(u), 1); u];
        U = eye(length(A)) - ((2 * (u*u'))/(norm(u,2)^2));
        Q = Q * U;
        A = U*A;
    end
R = A;
end

