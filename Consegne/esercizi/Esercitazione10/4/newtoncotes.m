function [ nc ] = newtoncotes( n )
% n inizialmente indice massimo dei nodi
n = n + 1; % ora n e' numero di alfa
nc = zeros(1,n);
for i = 1 : n
    funz = @(x)1;
    for j = 1 : n
        if (i ~= j)
            funz = @(x) funz(x).*((x-j)./(i-j));
        end
    end
    nc(i) = integral(funz,1,n);
end
end

