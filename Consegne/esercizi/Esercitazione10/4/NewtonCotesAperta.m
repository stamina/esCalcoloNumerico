function [ nc ] = NewtonCotesAperta(n)
% n inizialmente indice massimo dei nodi
nc = zeros(1,n);
if n==0 % matlab non sa integrare funzioni costanti
    nc = 2;
    return;
end
for i = 0 : n
    funz = @(x)1;
    for j = 0 : n
        if (i ~= j)
            funz = @(x) funz(x).*((x-j)./(i-j));
        end
    end
    nc(i+1) = integral(funz,-1,n+1);
end
end

