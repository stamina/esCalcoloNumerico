function [ area ] = NewtonCotesIterataAperta(a,b,intervalli,n,f)
    dist = (b-a)/intervalli;
    h = dist / (n+2);
    alfa = NewtonCotesAperta(n);
    area = 0;
    for i = 0 : intervalli-1
        x = linspace(a + i*dist,a+(i+1)*dist, n+3);
        areaint = h * sum(alfa .* f(x(2:n+2)));
        area = area + areaint;
    end

end

