A1 = [-3 3 -6; -4 7 -8; 5 7 -9];
A2 = [4 1 1; 2 -9 0; 0 -8 -6];

%%% JACOBI CON A1
M1 = diag(diag(A1));
N1 = M1-A1;
Pj1 = diag(1./diag(M1)) * N1;
raggioPj1 = max(abs(eig(Pj1)))
raggioPj1 =
    0.8133

%%% GAUSS-SEIDEL CON A1
E1 = -tril(A1,-1);
F1 = -triu(A1,+1);
D1 = M1;
Pgs1 = inv(D1-E1) * F1;
raggioPgs1 = max(abs(eig(Pgs1)))
raggioPgs1 =
    1.1111

%%% JACOBI CON A2
M2 = diag(diag(A2));
N2 = M2-A2;
Pj2 = diag(1./diag(M2)) * N2;
raggioPj2 = max(abs(eig(Pj2)))
raggioPj2 =
    0.4438

%%% GAUSS-SEIDEL CON A2
F2 = -triu(A2,+1);
E2 = -tril(A2,-1);
D2 = M2;
Pgs2 = inv(D2-E2) * F2;
raggioPgs2 = max(abs(eig(Pgs2)))
raggioPgs2 =
    0.0185

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Il raggio spettrale di P in Gauss-Seidel usando la matrice A1
% e' maggiore di 1. Questo significa che Gauss-Seidel in questo
% caso non convergera'.
% Possiamo usare il raggio spettrale come indice della velocita'
% di convergenza (che e' garantita se <1). Minore sara' il raggio
% spettrale, piu' velocemente il metodo usato sulla matrice convergera'. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[xA1j, errA1j] = Jacobi(A1,[-6 -5 3]', zeros(3,1),100,10^(-3));
xA1j
xA1j =
     9.996750822734677e-01
     1.000243188578632e+00
     9.999893341364083e-01

[xA1gs, errA1gs] = GaussSeidel(A1,[-6 -5 3]', zeros(3,1),100,10^(-3));
xA1gs
xA1gs =
     9.314846820357254e+08
     1.000000000000000e+00
     5.174914904642919e+08

[xA2j, errA2j] = Jacobi(A2,[6 -7 -14]', zeros(3,1),100,10^(-3));
xA2j
xA2j =
     1.000018787363038e+00
     1.000084087415160e+00
     1.000307183676081e+00

[xA2gs, errA2gs] = GaussSeidel(A2,[6 -7 -14]', zeros(3,1),100,10^(-3));
xA2gs
xA2gs =
     1.000003175328964e+00
     1.000000705628659e+00
     9.999990591617882e-01

A1 * xA1j 
ans =
    -5.998231685902955e+00
    -4.996912682134711e+00
     3.000173724190089e+00

A1 * xA1gs
ans =
    -5.899402985892927e+09
    -7.865870644857237e+09
     3.000000000000000e+00

A2 * xA2j
ans =
     6.000466420543391e+00
    -7.000719212010363e+00
    -1.400251580137776e+01

A2 * xA2gs
ans =
     6.000012466106304e+00
    -6.999999999999998e+00
    -1.400000000000000e+01

%% ERRORI FINALI RISPETTO AL PASSO PRECEDENTE

errA1gs(length(errA1gs))
ans =
     2.024602371120472e+09

errA2gs(length(errA2gs))
ans =
     1.794643108524222e-04

errA2j(length(errA2j))
ans =

     5.012968269660661e-04

errA1j(length(errA1j))
ans =

     9.061900745775138e-04

%% Gli errori ottenuti sono in tre casi minori della tolleranza.
%% Nel caso di A1 con GaussSedel sono stati raggiunti i passi massimi,
%% motivo per cui l'errore e' cosi' elevato.
%% Tutto cio' pero' non ci permette di dire nulla sulla convergenza dei due
%% metodi applicati alle matrici (questo per via del raggio spettr. > 1).

%% GRAFICI

hold on

subplot(2,2,1);
plot(1:length(errA1j),errA1j);

subplot(2,2,2);
plot(1:length(errA1gs),errA1gs);

subplot(2,2,3);
plot(1:length(errA2j),errA2j);

subplot(2,2,4);
plot(1:length(errA2gs),errA2gs);


%% TABELLE

scriptTabelle

Tabella - Jacobi A1
1 2.149724e+00 
2 9.441443e-01 
3 9.009817e-01 
4 7.436716e-01 
5 1.249872e-01 
6 1.053206e-01 
7 1.518949e-01 
8 3.129344e-02 
9 6.881143e-02 
10 3.659135e-02 
11 2.514329e-02 
12 3.010791e-02 
13 1.773172e-02 
14 1.667270e-02 
15 1.379642e-02 
16 1.032093e-02 
17 9.047043e-03 
18 7.103599e-03 
19 5.779165e-03 
20 4.778177e-03 
21 3.822120e-03 
22 3.136203e-03 
23 2.548183e-03 
24 2.065876e-03 
25 1.686336e-03 
26 1.368573e-03 
27 1.113545e-03 
28 9.061901e-04 

Tabella - GaussSeidel A1
1 2.327712e+00 
2 1.981613e+00 
3 2.183633e+00 
4 2.420894e+00 
5 2.688304e+00 
6 2.986540e+00 
7 3.318241e+00 
8 3.686895e+00 
9 4.096538e+00 
10 4.551705e+00 
11 5.057449e+00 
12 5.619388e+00 
13 6.243764e+00 
14 6.937516e+00 
15 7.708351e+00 
16 8.564834e+00 
17 9.516483e+00 
18 1.057387e+01 
19 1.174874e+01 
20 1.305416e+01 
21 1.450462e+01 
22 1.611625e+01 
23 1.790694e+01 
24 1.989660e+01 
25 2.210733e+01 
26 2.456371e+01 
27 2.729301e+01 
28 3.032556e+01 
29 3.369507e+01 
30 3.743897e+01 
31 4.159885e+01 
32 4.622094e+01 
33 5.135661e+01 
34 5.706289e+01 
35 6.340322e+01 
36 7.044802e+01 
37 7.827558e+01 
38 8.697286e+01 
39 9.663651e+01 
40 1.073739e+02 
41 1.193043e+02 
42 1.325604e+02 
43 1.472893e+02 
44 1.636548e+02 
45 1.818386e+02 
46 2.020429e+02 
47 2.244922e+02 
48 2.494357e+02 
49 2.771508e+02 
50 3.079453e+02 
51 3.421615e+02 
52 3.801794e+02 
53 4.224216e+02 
54 4.693573e+02 
55 5.215082e+02 
56 5.794535e+02 
57 6.438372e+02 
58 7.153747e+02 
59 7.948608e+02 
60 8.831786e+02 
61 9.813096e+02 
62 1.090344e+03 
63 1.211493e+03 
64 1.346104e+03 
65 1.495671e+03 
66 1.661856e+03 
67 1.846507e+03 
68 2.051675e+03 
69 2.279638e+03 
70 2.532932e+03 
71 2.814368e+03 
72 3.127076e+03 
73 3.474529e+03 
74 3.860588e+03 
75 4.289542e+03 
76 4.766158e+03 
77 5.295731e+03 
78 5.884145e+03 
79 6.537939e+03 
80 7.264377e+03 
81 8.071530e+03 
82 8.968366e+03 
83 9.964852e+03 
84 1.107206e+04 
85 1.230229e+04 
86 1.366921e+04 
87 1.518801e+04 
88 1.687556e+04 
89 1.875063e+04 
90 2.083403e+04 
91 2.314892e+04 
92 2.572102e+04 
93 2.857892e+04 
94 3.175435e+04 
95 3.528261e+04 
96 3.920290e+04 
97 4.355878e+04 
98 4.839864e+04 
99 5.377627e+04 

Tabella - Jacobi A2
1 2.880865e+00 
2 1.338467e+00 
3 5.082856e-01 
4 2.800925e-01 
5 9.183776e-02 
6 4.832052e-02 
7 2.509818e-02 
8 6.369207e-03 
9 4.672788e-03 
10 2.103686e-03 
11 5.012968e-04 

Tabella - GaussSeidel A2
1 2.051882e+00 
2 5.233179e-01 
3 9.691073e-03 
4 1.794643e-04 
