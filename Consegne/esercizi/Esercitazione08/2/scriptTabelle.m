fprintf('Tabella - Jacobi A1\n');
for i = 1 : length(errA1j)
    fprintf('%d %d \n',i,errA1j(i));
end
fprintf('\n');

fprintf('Tabella - GaussSeidel A1\n');
for i = 1 : length(errA1gs)
    fprintf('%d %d \n',i,errA1gs(i));
end
fprintf('\n');

fprintf('Tabella - Jacobi A2\n');
for i = 1 : length(errA2j)
    fprintf('%d %d \n',i,errA2j(i));
end
fprintf('\n');

fprintf('Tabella - GaussSeidel A2\n');
for i = 1 : length(errA2gs)
    fprintf('%d %d \n',i,errA2gs(i));
end
fprintf('\n');