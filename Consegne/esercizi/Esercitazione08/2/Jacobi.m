function [xnew,err] = Jacobi(A,b,x0,Nmax,toll)
    D = diag(diag(A));
    B = D - tril(A);
    C = D - triu(A);
    invD = diag(1./diag(A));
    J = invD*(B+C);
    xold = x0;
    i = 1;
    test=1;
    err=zeros(1,Nmax);
    while i<Nmax && test>toll
        xnew = J*xold+invD*b;
        err(i)= norm(xnew-xold);
        test = err(i) ;
        xold = xnew;
        i = i + 1;
    end
    err = err(1:i-1) ;
end