function [x,errore] = GaussSeidel(A,b,x0,Nmax,toll)
    D = diag(diag(A));
    B = D - tril(A);
    C = D - triu(A);
    invD = diag(1./diag(A));
    n = length(A);
    x = x0;
    i = 1;
    test=1;
    err = zeros(n,1);
    errore = zeros(1,Nmax);
    while i<Nmax && test>toll
        for k = 1 : length(A)
            vecchio = x(k);
            x(k) = invD(k,:)*b + invD(k,:)*B*[x(1:k-1); zeros(n-k+1,1)] ...
                + invD(k,:)*C*[zeros(k-1,1); x(k:n)];
            err(k)= x(k) - vecchio;
        end
        errore(i) = norm(err);
        test = errore(i) ;
        i = i + 1;
    end
    errore = errore(1:i-1) ;
end