function [L, U, P] = algLU(A)
    P = pivoting(A);
    A = P*A;
    L = eye(length(A));
    U = zeros(length(A));

    for i = 1: length(A)
      for j = i : length(A)
            somma = 0;
                for p = 1: i-1
                    somma = somma + L(i,p) * U(p,j);
                end
            U(i,j) = A(i,j) - somma;
      end
      for j = i+1 : length(A)
            somma = 0;
            for p = 1: i-1
                somma = somma + L(j,p) * U(p,i);
            end
            L(j,i) = (A(j,i) - somma) / U(i,i);
      end  
    end
end

