function [P] = pivoting(A)
    P = eye(length(A));
    for i = 1:length(A)
       if (A(i,i) == 0)
            [m,j] = max(abs(A(i:length(A),i)));
            % sto passando un vettore piu' piccolo
            j = i + j - 1;
            temp = A(i,:);
            A(i,:) = A(j,:);
            A(j,:) = temp;
            temp = P(i,:);
            P(i,:) = P(j,:);
            P(j,:) = temp;
       end
    end
end

