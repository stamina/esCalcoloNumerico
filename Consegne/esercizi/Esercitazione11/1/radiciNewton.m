function [x] = radiciNewton(f,x0,toll,maxPassi)
% Calcolo di una radice usando newton
% f deve essere una funzione con una var. simbolica
% si usa 'subs(f,x0)' per valutare f in x0
    fp = diff(f);
    i = 1;
    x(i) = x0;
    while (i == 1 || i < maxPassi && abs(x(i) - x(i-1)) > toll)
        i = i + 1;
        x(i) = x(i-1) - subs(f,x(i-1))/subs(fp,x(i-1));
    end
    x = x';
end

