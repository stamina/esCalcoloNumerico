massimo = max([length(a_rad),length(b_rad),length(c_rad),length(d_rad)]);
fprintf('|   A      |   B      |   C      |   D      | \n');
for i = 1 : massimo
    fprintf('|');
    if length(a_rad) >= i
        fprintf(' %-.6f |',a_rad(i));
    else
        fprintf(' -------- |');
    end
    if length(b_rad) >= i
        fprintf(' %-.6f |',b_rad(i));
    else
        fprintf(' -------- |');
    end
    if length(c_rad) >= i
        fprintf(' %-.6f |',c_rad(i));
    else
        fprintf(' -------- |');
    end
    if length(d_rad) >= i
        fprintf(' %-.6f |',d_rad(i));
    else
        fprintf(' -------- |');
    end
    fprintf('\n');
    
end