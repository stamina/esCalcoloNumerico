function [ err ] = maggErrNewtonCotes(f,a,b,n)
% Maggiorazione dell'errore usando NewtonCotes con n passato
% Calcolo dell'integrale di f nell'intervallo a,b.
h = (b-a)/n;
tau= (t) 1;
for i = 0:n
    tau = @(t) tau(t) * (t-i);
end

    if (mod(2,0) == 0) % pari
       der = f;
       for i = 1 : n+2
           der = diff(der);
       end
        
    else % dispari
       der = f;
       for i = 1 : n+1
           der = diff(der);
       end
    end


end

