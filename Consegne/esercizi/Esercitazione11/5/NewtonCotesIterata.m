function [ area ] = NewtonCotesIterata(f,a,b,intervalli,n)
    dist = (b-a)/intervalli;
    h = dist / n;
    alfa = newtoncotes(n);
    area = 0;

    for i = 0 : intervalli-1
        x = linspace(a + i*dist,a+(i+1)*dist, n+1);
        areaint = h * sum(alfa .* subs(f,x));
        area = area + areaint;
    end

end

