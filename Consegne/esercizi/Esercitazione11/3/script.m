p = input('Coefficienti?\n');
disp('Calcolo la matrice di Frobenius:');
F = creaFrobenius(p)
disp('Calcolo gli autovaori di F:');
autovalori = eig(F)
disp('Verifico le radici trovate:')
polyval(p,autovalori)
disp('Certo i limiti infer. e supior. entro cui trovare le radici:');
[minv,maxv] = ricercaX0(F)
disp('Certo la radice minima:');
alfamin = radiceNewton(p,minv-1,10^-6,300)
disp('Certo la radice massima:');
alfamax = radiceNewton(p,maxv+1,10^-6,300)