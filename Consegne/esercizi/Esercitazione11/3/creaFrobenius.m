function [ A] = creaFrobenius(f)
% dato f vettore di coeffic. ritorna la sua
% matrice di Frobenius
n = length(f)-1;
A = zeros(n,n) + diag(ones(n-1,1),-1);
for i = 1 : n
    A(i,n) = - f(n+2-i)/f(1); 
end
end

