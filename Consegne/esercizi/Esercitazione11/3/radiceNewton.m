function [ alfa ] = radiceNewton(p, x,  toll, maxPassi)
% prende in input i coeff. di un polinomio e restituisce le sue radici
    pPrimo = polyder(p);
    alfa = [];
        precedente = -Inf;
        attuale = x;
        prove = 0;
        while (prove <= maxPassi && abs(precedente - attuale) > toll)
            precedente = attuale;
            attuale = precedente - polyval(p,precedente)/...
                (polyval(pPrimo,precedente));
            prove = prove + 1;
        end
        if (prove >= maxPassi)
            return;
        end
        alfa = attuale;
end

