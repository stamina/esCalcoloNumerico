function [err] = erroreIntegr(n)
    funzione = @(x) exp(-x.^2);
    err = zeros(1,n);
        for i = 1 : n
            alfa = newtoncotes(i);
            xx = linspace(0,1,i+1);
            yy = funzione(xx);
            integrAppross = 1/i * sum(alfa .* yy);
            integrVera = integral(funzione,0,1);
            err(i) = abs(integrAppross - integrVera);
        end
end

