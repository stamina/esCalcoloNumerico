a = 0;
b = 1;
j = 1;
f = @(x) sin(x);
integrale = integral(f, a, b);
for i = 10:10:100
    apprTrap(j) = NewtonCotesIterata(a,b,i,1,f);
    apprCavSim(j) = NewtonCotesIterata(a,b,i,2,f);
    j = j+1;
end
erroreTrap = abs(integrale - apprTrap)/integrale;
erroreCavSim = abs(integrale - apprCavSim)/integrale;