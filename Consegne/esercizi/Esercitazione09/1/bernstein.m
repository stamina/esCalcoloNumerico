function [ber]=bernstein(k,n)
    i=0;
    if k == 0
        coef = 1;
    else
        coef=prod([1:n])/(prod([1:k])*prod([1:n-k]));
    end
    for t=0:0.01:1
    i=i+1;
    ber(i)=coef*t^k*(1-t)^(n-k);
    end
end