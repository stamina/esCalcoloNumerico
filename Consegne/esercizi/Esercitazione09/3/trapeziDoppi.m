function [ integrale ] = trapeziDoppi(f,a,b,c,d)
    hX = d-c;
    hY = b-a;
    integrale = (f(a,c) + f(a, d) + f(b,c) + f(b,d)) * ...
        ((1/2 * hX) * (1/2 * hY));
end

