function [ integrale ] = trapeziDoppiIterato(f,a,b,c,d,n,m)
    hX = (b-a)/n;
    hY = (d-c)/m;
    integrale = 0;

    for i = 0: n-1
        for j = 0 : m-1
            integrale = integrale + ...
                trapeziDoppi(f,a+(i*hX), a+((i+1)*hX),c+(j*hY), c+((j+1)*hY));
        end
    end
end

