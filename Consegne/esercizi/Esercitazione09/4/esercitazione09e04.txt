A1 = [-3 3 -6; -4 7 -8; 5 7 -9];
A2 = [7 4 -7;4 5 -3;-7 -3 8];

% Calcolo degli omega che minimizzino il raggio spettrale

[Omega1, raggioSpettrale1] = miglioreOmega(A1,1000)

Omega =
    0.8849

raggioSpettrale =
    0.6107

[Omega2, raggioSpettrale2] = miglioreOmega(A2,1000)
Omega =
    1.5315

raggioSpettrale =
    0.6617

% Nei casi considerati questi omega sono quelli che minimizzano
% il raggio spettrale di P.
b1 = [-6 -5 3]';
b2 = [4 6 -2]';
[soluz11,passi11]= rilassamento(A1,b1,zeros(3,1),Omega1,10^(-6),1000)

soluz11 =
    1.0000
    1.0000
    1.0000

passi11 =
    30

[soluz12,passi12]= rilassamento(A1,b1,zeros(3,1),0.1,10^(-6),1000)

soluz12 =
    1.0000
    1.0000
    1.0000

passi12 =
   250

[soluz21,passi21]= rilassamento(A2,b2,zeros(3,1),Omega2,10^(-6),1000)

soluz21 =
    1.0000
    1.0000
    1.0000

passi21 =
    45

[soluz22,passi22]= rilassamento(A2,b2,zeros(3,1),0.1,10^(-6),1000)

soluz22 =
    0.9685
    1.0110
    0.9758

passi22 =
        1000

% Sono stati raggiunti i mille passi massimi concessi.
% Si sarebbe potuto andare oltre col calcolo introducendo
% un max passi maggiore.

[soluz22,passi22]= rilassamento(A2,b2,zeros(3,1),0.1,10^(-6),10000)

soluz22 =
    1.0000
    1.0000
    1.0000

passi22 =
        3738

% Imponendo come numero passi massimo 10.000 si giunge
% alla soluzione approssimata in 3738 passi.

