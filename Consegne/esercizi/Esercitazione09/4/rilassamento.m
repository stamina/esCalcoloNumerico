function [x, passi] = rilassamento(A,b,x,omega,toller,maxPassi)
% Metodo del rilassamente
    D = diag(diag(A));
    E = -tril(A,-1);
    F = -triu(A,+1);
    P =  inv(D-(omega*E)) * (D + (omega*(F-D)));
    Q = omega * inv(D - omega*E) * b;
    err = inf;
    passi = 0;
    while (passi < maxPassi && err > toller)
        x = P*x + Q;
        err = norm(b - A*x);
        passi = passi + 1;
    end
end

