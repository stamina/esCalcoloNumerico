function [ minOmega, minRaggio] = miglioreOmega(A,numProve)
% calcolo dell'omega che minimizza il raggio sprettr. di P.
    D = diag(diag(A));
    E = -tril(A,-1);
    F = -triu(A,+1);
    omega = linspace(0+eps,2-eps,numProve);
    minRaggio = Inf;
    minOmega = 0;
    for i = omega
        P = inv(D-(i*E)) * (D + (i*(F-D)));
        raggiospettr = max(abs(eig(P)));
        if  (raggiospettr <= minRaggio)
            minRaggio = raggiospettr;
            minOmega = i;
        end
    end
end

