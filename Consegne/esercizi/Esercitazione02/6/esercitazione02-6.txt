B = (reshape([1:12],4,3))'
B =
     1     2     3     4
     5     6     7     8
     9    10    11    12

% size(B);
% ritorna le dimensioni di B

size(B)
ans =

     3     4

% C=B.*B;
% moltiplica B elemento per elemento
C=B.*B
C =

     1     4     9    16
    25    36    49    64
    81   100   121   144

% C=B*B;
% dovrebbe effettuare la moltiplicazione matriciale,
% ma il numero delle colonne della prima matrice e' diverso
% dal numero delle righe della prima matrice.

C=B*B;
%%%% errore!

% C=B'*B
% moltiplicazione matriciale fra la trasposta di B e B.

C=B'*B
C =
   107   122   137   152
   122   140   158   176
   137   158   179   200
   152   176   200   224

% B(1:2,4),B(:,3),B(1:2,:),B(:,[2 4]),B([2 3 3])
% ritorna 5 matrici diverse:
% - la prima, contiene della prima e della seconda riga la quarta colonna
% - la seconda, contiene la terza colonna
% - la terza, contiene la prima e la seconda riga
% - la quarta, contiene la seconda e quarta colonna
% - la quinta, contiene gli elementi all'indice di B 2, 3 e 3.

B(1:2,4),B(:,3),B(1:2,:),B(:,[2 4]),B([2 3 3])
ans =
     4
     8

ans =
     3
     7
    11

ans =
     1     2     3     4
     5     6     7     8

ans =
     2     4
     6     8
    10    12

ans =
     5     9     9

% B(3,2) = B(1,1)
% cambia il valore alla terza riga e seconda colonna e lo rende
% il valore che e' presente alla prima riga prima colonna.

B(3,2) = B(1,1)
B =
     1     2     3     4
     5     6     7     8
     9     1    11    12

% A(1:2,4)=zeros(2,1)
% rende gli elementi della prima riga quarta colonna
% e seconda riga quarta colonna, uguali a zero.

B(1:2,4)=zeros(2,1)
B =

     1     2     3     0
     5     6     7     0
     9     1    11    12

% B(2,:)=B(2,:)-B(2,1)/B(1,1)*B(1,:);
% applica gauss sulla seconda riga partendo dalla prima

B(2,:)=B(2,:)-B(2,1)/B(1,1)*B(1,:)
B =

     1     2     3     0
     0    -4    -8     0
     9     1    11    12

