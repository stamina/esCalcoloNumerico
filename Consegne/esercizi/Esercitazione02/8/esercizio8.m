function [radici] = esercizio8( alfa )
    p = 10.^alfa;
    b = (1 + p.^2)./p;

    delta = (-b).^2 - 4;
    radici = zeros(4,length(alfa));
    radici(1,:) = - sqrt((b - sqrt(delta))/2);
    radici(2,:) = + sqrt((b - sqrt(delta))/2);
    radici(3,:) = - sqrt((b + sqrt(delta))/2);
    radici(4,:) = + sqrt((b + sqrt(delta))/2); 
end

